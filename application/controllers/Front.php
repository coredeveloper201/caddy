<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper( 'url' );
		$this->load->database();
		$this->load->model( 'user_model' );
		$this->load->library( 'session' );
	}


	public function index() {
		$this->load->view( 'front/layouts/header' );
		$data['final'] = $this->user_model->get_card();
		$this->load->view( 'front/front_view', $data );

		$this->load->view( 'front/layouts/footer' );
	}

	public function signup() {
		if ( $this->session->userdata( 'email' ) && ( $this->session->userdata( 'type' ) !== 'admin' ) ) {
			redirect( 'account' );
		} else {
			$this->load->view( 'front/layouts/header' );
			$this->load->view( 'front/signup' );
			$this->load->view( 'front/layouts/footer' );
		}
	}

	public function login() {
		if ( $this->session->userdata( 'email' ) && ( $this->session->userdata( 'type' ) !== 'admin' ) ) {
			redirect( 'account' );
		} else {
			$this->load->view( 'front/layouts/header' );
			$this->load->view( 'front/login' );
			$this->load->view( 'front/layouts/footer' );
		}
	}

	public function forgottenPassword() {
		$this->load->view( 'front/forgotten_password' );


	}

	public function about_us() {

		$this->load->view( 'front/layouts/header-about' );
		$this->load->view( 'front/about_us' );
		$this->load->view( 'front/layouts/footer' );
	}


	public function aboutus() {
		$url = $this->uri->segment( 1 );

		$this->db->select( '*' );
		$this->db->where( 'slug', $url );
		$q = $this->db->get( 'pages' );
		$res['data'] = $q->row();
		// echo "<pre>"; print_r($data); die();
		$this->load->view( 'front/layouts/header-about' );
		$this->load->view( 'front/about_us', $res );
		$this->load->view( 'front/layouts/footer' );
	}

	public function pages() {
		$url = $this->uri->segment( 2 );
		// echo "<pre>"; print_r($url);
		$this->db->select( '*' );
		$this->db->where( 'slug', $url );
		$q           = $this->db->get( 'pages' );
		$res['data'] = $q->row();
		// echo "<pre>"; print_r($res['data']); die();
		$this->load->view( 'front/layouts/header' );
		$this->load->view( 'front/about_us', $res );
		$this->load->view( 'front/layouts/footer' );
	}

	public function contactus() {
		$url = $this->uri->segment( 1 );
		$this->db->select( '*' );
		$this->db->where( 'slug', $url );
		$q           = $this->db->get( 'pages' );
		$res['data'] = $q->row();
		//echo "<pre>"; print_r($data); die();
		$this->load->view( 'front/layouts/header' );
		$this->load->view( 'front/contact-us' );
		$this->load->view( 'front/layouts/footer' );
	}

	public function add_user() {

		// if($_FILES["upload_file"]['name']!=""){


		// $config['upload_path']          = './assets/site/images/';
		// $config['allowed_types']        = 'gif|jpg|png|pdf|doc|txt|csv|xml|mp3|mp4';
		// $config['overwrite']            = TRUE;
		// $config['max_size']             = 2048000; // Can be set to particular file size , here it is 2 MB(2048 Kb)
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		// $this->load->library('upload', $config);

		// if ( ! $this->upload->do_upload('upload_file'))
		//   {
		//           $error = array('error' => $this->upload->display_errors());

		//           // echo '<pre>';
		//           // print_r($error);
		//           // die;

		//           // $this->load->view('manage_profile', $error);
		//   	   $this->session->set_flashdata('error', $error);
		//   }else{

		// $image_data = $this->upload->data();
		// $image = time().$image_data['file_name'];

		$this->db->select( '*' );
		$this->db->from( 'clients_web' );
		$this->db->where( 'email', $this->input->post( 'email' ) );
		$query = $this->db->get();
		$res   = $query->row_array();

		if ( $res > 0 ) {

			$this->session->set_flashdata( 'error', 'Email Already exits please enter a unique Email. ' );
			redirect( 'sign-up' );
			die;
		}


		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'firstname', 'Firstname', 'required' );
		$this->form_validation->set_rules( 'lastname', 'Lastname', 'required' );
		$this->form_validation->set_rules( 'email', 'Email', 'trim|required|valid_email' );
		$this->form_validation->set_rules( 'password', 'Password', 'required' );
		$this->form_validation->set_rules( 'country', 'Country', 'required' );
		$this->form_validation->set_rules( 'phone', 'phone Number ', 'required|regex_match[/^[0-9]{10}$/]' );
		//$this->form_validation->set_rules('telegram', 'Telegram', 'required');
		$this->form_validation->set_rules( 'hear_about_us', 'Hear about us', 'required' );
		$this->form_validation->set_rules( 'daily_amount', 'Daily amount needed', 'required' );
		if ( $this->form_validation->run() ) {
			$data = array(
				'firstname'     => $this->input->post( 'firstname' ),
				'lastname'      => $this->input->post( 'lastname' ),
				'email'         => $this->input->post( 'email' ),
				'password'      => md5( $this->input->post( 'password' ) ),
				'country'       => $this->input->post( 'country' ),
				'phone'         => $this->input->post( 'phone' ),
				'telegram'      => $this->input->post( 'telegram' ),
				'hear_about_us' => $this->input->post( 'hear_about_us' ),
				'daily_amount'  => $this->input->post( 'daily_amount' ),
				//'upload_file'=>$image, // add this for image
				"created"       => date( 'Y-m-d H:i:s' )
			);

			//echo '<pre>';print_r($data); die();
			$this->user_model->insert_users( $data );
			$this->sendEmail( $data );
			$this->session->set_flashdata( 'success', 'Thank you for your request, we are processing ASAP, you will hear back shortly. </br> or please join our <a href="https://t.me/thecardcaddy" target="_blank" class="light-purple"><i class="fa fa-telegram"></i> Telegram</a> group to hear about awesomeness. </br>Didn\'t get the email? Please try to check the spam folder else connect us on support@cardcaddy.co.' );
			redirect( 'login' );


		} else {
			//echo "error";

			$this->session->set_flashdata( 'error', 'you have enter some wrong inputs in signup.' );
			redirect( 'sign-up' );
		}
		//}
		// }else{
		// 						$data = array(
		// 						'firstname'    => $this->input->post('firstname'),
		// 						'lastname'     => $this->input->post('lastname'),
		// 						'email'      => $this->input->post('email'),
		// 						'password'      => md5($this->input->post('password')),
		// 						'country'      => $this->input->post('country'),
		// 						'phone'      => $this->input->post('phone'),
		// 						'telegram'      => $this->input->post('telegram'),
		// 						'hear_about_us'      => $this->input->post('hear_about_us'),
		// 						'daily_amount'      => $this->input->post('daily_amount'),
		// 						//'upload_file'=>$image, // add this for image
		// 						"created" => date('Y-m-d H:i:s')
		// 				);

		// 				echo '<pre>';print_r($data); die();
		// 				$this->user_model->insert_users($data);
		// 				$this->session->set_flashdata('success', 'signup successfully now enter to login.');
		// 				redirect('login');
		// }


	}

	public function contact_us() {

		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'fullname', 'Fullname', 'required' );
		$this->form_validation->set_rules( 'email', 'Email', 'trim|required|valid_email' );
		$this->form_validation->set_rules( 'phone', 'phone Number ', 'required|regex_match[/^[0-9]{10}$/]' );
		$this->form_validation->set_rules( 'message', 'Message', 'required' );
		if ( $this->form_validation->run() ) {
			$data = array(
				'fullname' => $this->input->post( 'fullname' ),
				'email'    => $this->input->post( 'email' ),
				'phone'    => $this->input->post( 'phone' ),
				'message'  => $this->input->post( 'message' )
			);

			//echo '<pre>';print_r($data); die();
			$this->user_model->insert_contact( $data );
			$this->session->set_flashdata( 'success', 'Thanks for Contacting us we will contact you soon' );
			redirect( '/thank-you' );

		} else {

			$this->session->set_flashdata( 'error', 'you have enter some wrong inputs in contact us please check' );
			redirect( 'contact-us' );
		}


	}


	public function thank_you() {
		$this->load->view( 'front/layouts/header' );
		$this->load->view( 'front/thankyou' );
		$this->load->view( 'front/layouts/footer' );
	}

	public function user_login() {
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'email', 'Email', 'required' );
		$this->form_validation->set_rules( 'password', 'Password', 'required' );
		if ( $this->form_validation->run() ) {
			//true
			$email    = $this->input->post( 'email' );
			$password = md5( $this->input->post( 'password' ) );
			//model function
			if ( $this->user_model->can_login( $email, $password ) ) {
				$res = $this->user_model->getall( $email );

				if ( isset( $res->activated_at ) ) {
					$session_data = array(
						'type'         => "user",
						'email'        => $email,
						'id'           => $res->id,
						'firstname'    => $res->firstname,
						'lastname'     => $res->lastname,
						'phone'        => $res->phone,
						'telegram'     => $res->telegram,
						'daily_amount' => $res->daily_amount,

					);
					$this->session->set_userdata( $session_data );
					redirect( 'account' );
				} else {
					$this->session->set_flashdata( 'error', 'Thank you for your request, we are processing ASAP, you will hear back shortly.
or please join our <a href="https://t.me/thecardcaddy" target="_blank" class="light-purple">
            <i class="fa fa-telegram"></i>Telegram
          </a> group to hear about awesomeness. </br>Didn\'t get the email? Please try to check the spam folder else connect us on support@cardcaddy.co.' );
					redirect( 'login' );
				}
			} else {
				$this->session->set_flashdata( 'error', 'Invalid Username and Password please enter a valid details' );
				redirect( 'login' );
			}
		} else {
			$this->session->set_flashdata( 'error', 'please fill all the fields to Enter Club' );
			redirect( 'login' );
		}
	}


	public function account_page() {
		if ( ! $this->session->userdata( 'email' ) ) {
			redirect( '/' );
		} else {
			$this->load->view( 'front/layouts/header' );
			$this->load->view( 'front/user_account' );
			$this->load->view( 'front/layouts/footer' );
		}
	}

	public function logout() {
		$this->session->unset_userdata( 'email' );
		$this->session->sess_destroy();
		redirect( '/' );
	}


	public function giftCard() {
		if ( ! $this->session->userdata( 'email' ) && ( $this->session->userdata( 'type' ) !== 'admin' ) ) {
			redirect( '/' );
		} else {
			$this->load->view( 'front/layouts/header' );
			$this->load->view( 'front/gift_card' );
			$this->load->view( 'front/layouts/footer' );
		}
	}

	public function get_slug() {

		$this->user_model->about_us();

	}

	public function get_data() {

		$data = $this->user_model->users();

		echo "<pre>";
		print_r( $data );
		die();

	}

	public function update_password() {


		$id = $this->session->userdata( 'id' );
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'password', 'Password', 'required' );
		if ( $this->form_validation->run() ) {
			$new_password = $this->input->post( 'password' );
			$data         = array( 'password' => md5( $new_password ) );

			$this->db->where( 'id', $id );
			$this->db->update( 'clients_web', $data );

			$this->session->set_flashdata( 'success', 'Password change sucessfully' );


			redirect( '/account' );

		} else {
			$this->session->set_flashdata( 'error', 'Invalid Username and Password please enter a valid details' );


			redirect( 'front/changePassword' );

		}

	}

	public function updatePassword() {

		if ( ! $this->session->userdata( 'email' ) ) {
			redirect( 'front' );
		} else {
			$this->load->view( 'front/layouts/header' );
			$this->load->view( 'front/changePasword' );
			$this->load->view( 'front/layouts/footer' );
		}

	}


	public function reset_password() {

		$this->load->view( 'front/layouts/header' );
		$this->load->view( 'front/reset_password' );
		$this->load->view( 'front/layouts/footer' );

	}


	public function forgotPassword() {

		$email = $this->input->post( 'email' );

		// echo '<pre>';
		// print_r($email); die();

		$find_email = $this->user_model->ForgotPassword( $email );

		if ( ! $find_email ) {

			$this->session->set_flashdata( 'msg', ' Email not found please enter a valid email!' );
			redirect( 'reset-password' );
			// redirect(base_url().'user/Login','refresh');
		} else {

			// echo "found";
			$this->sendpassword( $find_email );

			//      echo '<pre>';
			// print_r($data); die();


		}


	}

	public function sendEmail( $data ) {


		$email = $data['email'];

		$mail_message = 'Dear ' . $data['firstname'] . ',' . "\r\n";
		$mail_message .= 'Thank you for your request, we are processing ASAP, you will hear back shortly.';
		$mail_message .= "\r\n";
		$mail_message .= 'Or';
		$mail_message .= "\r\n";
		$mail_message .= "Please join our Telegram group to hear about awesomeness";
		$mail_message .= "\r\n";
		$mail_message .= "Click the link: https://t.me/thecardcaddy";
		$mail_message .= "\r\n";
		$mail_message .= 'Thanks & Regards';
		$mail_message .= "\r\n";
		$mail_message .= 'Cardcaddy';

		$config = array(
			'mailtype' => 'html',
			'charset'  => 'utf-8',
			'priority' => '1'
		);
		$this->load->library( 'email', $config );

		$this->email->set_newline( "\r\n" );

		$this->email->from( 'support@cardcaddy.co', 'Cardcaddy' );
		$this->email->to( $email );

		$this->email->subject( 'From Cardcaddy' );
		$this->email->message( $mail_message );
		$this->email->send();


	}
	public function sendpassword( $data ) {

		$email = $data['email'];
		$sql   = $this->db->query( "SELECT *  from clients_web where email = '" . $email . "' " );
		$row   = $sql->result_array();
		if ( $sql->num_rows() > 0 ) {

			$passwordplain       = "";
			$passwordplain       = rand( 999999999, 9999999999 );
			$newpass['password'] = md5( $passwordplain );
			$this->db->where( 'email', $email );
			$this->db->update( 'clients_web', $newpass );
			$mail_message = 'Dear ' . $row[0]['firstname'] . ',' . "\r\n";
			$mail_message .= 'Thanks for contacting regarding to forgot password,</br> Your <b>Password</b> is <b>' . $passwordplain . '</b>' . "\r\n";
			$mail_message .= '</br>Please Update your password.';
			$mail_message .= '</br>Thanks & Regards';
			$mail_message .= '</br>Cardcaddy';

			$config = array(
				'mailtype' => 'html',
				'charset'  => 'utf-8',
				'priority' => '1'
			);


			$this->load->library( 'email', $config );

			$this->email->set_newline( "\r\n" );

			//Add file directory if you need to attach a file
			//$this->email->attach($file_dir_name);

			$this->email->from( 'support@cardcaddy.co', 'Cardcaddy' );
			$this->email->to( $email );

			$this->email->subject( 'From Cardcaddy' );
			$this->email->message( $mail_message );

			if ( $this->email->send() ) {
				//Success email Sent
				$this->session->set_flashdata( 'success', 'A new password willl be send to your email id please check.' );
				redirect( 'reset-password' );
			} else {
				//Email Failed To Send
				echo "<script>alert('Failed to send password, please try again!')</script>";
			}


		} else {
			echo "error";
		}

	}


	public function get_users() {

		$this->db->select( '*' );
		$this->db->from( 'clients_web' );
		$query      = $this->db->get();
		$final_data = $query->result_array();
		echo '<pre>';
		print_r( $final_data );
		die();

	}


	public function discount1() {

		$this->load->view( 'front/layouts/header' );
		$this->load->view( 'front/buy_discount' );
		$this->load->view( 'front/layouts/footer' );
	}

	public function discount() {
		$url = $this->uri->segment( 2 );
		// echo "<pre>"; print_r($url);
		$url = str_replace( '-', ' ', $url );
		$this->db->select( '*' );
		$this->db->where( 'title', $url );
		$q            = $this->db->get( 'gift_card' );
		$res['card']  = $q->row();
		$res['title'] = $url;
		//echo "<pre>"; print_r($res['card']); die();


		$this->db->select( '*' );
		$this->db->where( 'gift_id', $res['card']->id );

		$q             = $this->db->get( 'discount_coupons' );
		$res['coupon'] = $q->result();

		//echo "<pre>"; print_r($res['coupon']); die();

		$this->load->view( 'front/layouts/header' );
		$this->load->view( 'front/buy_discount', $res );
		$this->load->view( 'front/layouts/footer' );
	}


	    public function upload_page(){
		    if(!$this->session->userdata('email')){
			    redirect('front');
		    }
		    else
		    {
		    	$id = $this->session->userdata( 'id' );
			    $this->db->select( '*' );
			    $this->db->from( 'clients_web' );
			    $this->db->where( 'id', $id );
			    $query = $this->db->get();
			    $res['user']   = $query->row_array();
			    $this->load->view('front/layouts/header');
			    $this->load->view('front/upload_doc',$res);
			    $this->load->view('front/layouts/footer');
		    }
		}

    public function upload_attachments_by_ajax () {
      $config['upload_path']          = './assets/user/images/';
      $config['overwrite']            = TRUE;
      $config['allowed_types']        = '*';
      $config['max_size']             = 2048000; // Can be set to particular file size , here it is 2 MB(2048 Kb)
      $original_image_name = $_FILES["image"]['name'];
      $new_name = time().$original_image_name;
      $config['file_name'] = $new_name;
      $this->load->library('upload', $config);
      if ( ! $this->upload->do_upload('image'))
      {
        $error = "File cannot be uploaded, please upload image file.";
        $this->session->set_flashdata('error', $error);
        echo json_encode(['error' => $error], 401);
        return ;
      } else
      {
        $image_data = $this->upload->data();
        $image = $image_data['file_name'];
        $id = $this->session->userdata( 'id' );
        $data = array(
          'user_id' => $id,
          'path'=> $image,
          'name' => $original_image_name,
        );
        $attachments = $this->user_model->insert_attachment($original_image_name, $image, $id);
        echo json_encode($attachments, 201);
      }
    }
    public function attachments_by_clients_web_id()
    {
      $id = $this->session->userdata( 'id' );
      $attachments = $this->user_model->attachments_by_clients_web_id($id);
      echo json_encode($attachments);
    }
    public function delete_attachment() {
      $id = $this->input->post( 'id') ;
      $this->db->select( '*' );
      $this->db->where( 'id',  $id);
      $q = $this->db->get( 'attachments' );
      $first = $q->row();
      $this->db->delete('attachments', array('id' => $id));
      if($this->db->affected_rows() >= 1){
          $upload_path = './assets/user/images/' . $first->path;
          if(unlink($upload_path)) {
            $this->attachments_by_clients_web_id();
          }
      } else {
          $this->attachments_by_clients_web_id();
      }
      return;
      /*
      if($this->db->affected_rows() >= 1){
          if(unlink($path))
          return TRUE;
      } else {
          return FALSE;
      }
      */

      // $this->db->delete('attachments', array('id' => $id));
      // return $this->attachments_by_clients_web_id($clients_web_id);
    }
    public function upload_doc() {
        $id = $this->session->userdata( 'id' );
        $data = array(
          'heading' => $this->input->post('heading'),
        );
        $this->db->where( 'id', $id );
        $this->db->update( 'clients_web', $data );
        $this->session->set_flashdata( 'success', 'All updated successfully' );
        redirect( '/account/upload' );
    }

    public function getAllUpload() {
     $this->db->select('*');
     $this->db->from('user_upload');
     $this->db->order_by("id", "desc");
     $query=$this->db->get();
     $attachments =  $query->result_array();
     $r = [];
     foreach ($attachments as $value) {
       $id = explode(',', $value['attachments']);
       $this->db->select('*');
       $this->db->from('attachments');
       $this->db->where_in('id', $id);
       $value['file_list'] = $this->db->get();
       $value['file_list'] = $value['file_list']->result_array();
       $r[] = $value;
     }
     // echo json_encode();
     echo json_encode($r);
    }

    public function upload_doc_ajax() {
        $id = $this->session->userdata( 'id' );
        $heading = $this->input->post('heading');
        $attachments = $this->input->post('attachments');

        $data = array(
          'heading' => $heading,
          'attachments' => $attachments,
          'clients_web_id' => $id
        );

       $this->db->insert('user_upload', $data);
       echo json_encode(['status' => 200]);
       return;
    }

		public function upload_doc_original_for_back_up() {
			$config['upload_path']          = './assets/user/images/';
			$config['allowed_types']        = 'gif|jpg|png|pdf|doc|txt|csv|xml|mp3|mp4';
			$config['overwrite']            = TRUE;
			$config['max_size']             = 2048000; // Can be set to particular file size , here it is 2 MB(2048 Kb)
			$config['max_width']            = 1024;
			$config['max_height']           = 768;

			$new_name = time().$_FILES["image"]['name'];
			$config['file_name'] = $new_name;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('image'))
			{
				//$error = array('error' => $this->upload->display_errors());
				$error = "File cannot be uploaded, please upload image file.";
				$this->session->set_flashdata('error', $error);
				redirect( '/account/upload' );
			}else{
				$image_data = $this->upload->data();
				$image = $image_data['file_name'];
				$data = array(
					'heading' => $this->input->post('heading'),
					'upload_file'=>$image,
				);

				$id = $this->session->userdata( 'id' );
				$this->db->where( 'id', $id );
				$this->db->update( 'clients_web', $data );

				$this->session->set_flashdata( 'success', 'File Uploaded Successfully.' );
				redirect( '/account/upload' );

			}
		}


}
