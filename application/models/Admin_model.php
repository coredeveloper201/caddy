<?php

class Admin_model extends CI_Model
{

     public function can_login($email, $password)
     {
          $this->db->where('email', $email);
          $this->db->where('password', $password);
          $query = $this->db->get('admin');
          //SELECT * FROM users WHERE username = '$username' AND password = '$password'
          if($query->num_rows() > 0)
          {
               return true;
          }
          else
          {
               return false;
          }
     }

     public function getall($email)
      {
       $this->db->select('*');
       $this->db->where("email",$email);
        $q=$this->db->get('admin');
       return $q->row();
     }

     public function fetch_user_by_id($id='')
     {
       $this->db->select('*');
       $this->db->where("id",$id);
       $q=$this->db->get('admin');
       return $q->row();
     }

     public function ForgotPassword($email){
       $this->db->select('email');
       $this->db->from('admin');
       $this->db->where('email', $email);
       $query=$this->db->get();
        return $query->row_array();

          //   echo '<pre>';
          // print_r($data); die();
     }

     public function list(){

      $this->db->select('*');
      $this->db->from('pages');
      $query=$this->db->get();
      return $final_data = $query->result_array();
      //  echo '<pre>'; print_r($final_data); die();

     }

     public function delete_data($id){

       $this->db->where('id', $id);
       $this->db->delete('pages');

    }

    public function fetch_page_by_id($id)
    {
      $this->db->select('*');
      $this->db->where('id',$id);
      $q= $this->db->get('pages');
      return $result =  $q->row_array();
       // echo '<pre>'; print_r($data); die();

    }

    public function users(){

     $this->db->select('*');
     $this->db->from('clients_web');
     $query=$this->db->get();
     return $final = $query->result_array();
     //  echo '<pre>'; print_r($final_data); die();

    }

    public function get_contactus(){

     $this->db->select('*');
     $this->db->from('contact_us');
     $query=$this->db->get();
     return $total = $query->result_array();
     //echo '<pre>'; print_r($final_data); die();

    }

   public function delete_user_data($id){

       $this->db->where('id', $id);
       $this->db->delete('clients_web');

    }

    public function delete_contact_data($id){

       $this->db->where('id', $id);
       $this->db->delete('contact_us');

    }


    public function get_card(){

     $this->db->select('*');
     $this->db->from('gift_card');
     $query=$this->db->get();
      return $final = $query->result_array();
      // echo '<pre>'; print_r($final); die();

    }



   public function get_coupon(){

    $this->db->select('*');
    $this->db->from('discount_coupons');
    $query=$this->db->get();
    return $final_data = $query->result_array();
        //echo '<pre>'; print_r($final_data); die();
   }

   public function delete_coupon($id){

       $this->db->where('id', $id);
       $this->db->delete('discount_coupons');

    }


   public function fetch_get_coupon_by_id($id)
   
        {
          $this->db->select('*');
          $this->db->where('id',$id);
          $q= $this->db->get('discount_coupons');
          return $result =  $q->row_array();
           // echo '<pre>'; print_r($data); die();

        }


   public function fetch_giftcard_by_id($id)
    {
      $this->db->select('*');
      $this->db->where('id',$id);
      $q= $this->db->get('gift_card');
      return $result =  $q->row_array();
       // echo '<pre>'; print_r($data); die();

    }

        public function delete_giftcard_data($id){

       $this->db->where('id', $id);
       $this->db->delete('gift_card');

    }


    public function get_totalCard(){

     $this->db->select('*');
     $this->db->from('gift_card');
     $query=$this->db->get();
      return $totalCard = $query->result_array();
      // echo '<pre>'; print_r($final); die();

    }

   public function get_totalCoupon(){

    $this->db->select('*');
    $this->db->from('discount_coupons');
    $query=$this->db->get();
    return $totalCoupon = $query->result_array();
        //echo '<pre>'; print_r($final_data); die();
   }




 }
?>
