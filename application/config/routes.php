<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'front';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin'] = 'admin_controller';
$route['dashboard'] = 'admin_controller/login';
$route['add-page'] = 'admin_controller/page';
$route['pages'] = 'admin_controller/list_page';
$route['delete/(:num)'] = 'admin_controller/delete_page/$1';
$route['add-page/(:num)'] = 'admin_controller/page/$1';
$route['signup'] = 'front/add_user';
$route['login'] = 'front/login';
$route['logout'] = 'front/logout';
$route['contact-us'] = 'front/contactus';
$route['thank-you'] = 'front/thank_you';
$route['users'] = 'admin_controller/list_users';
$route['qutoes'] = 'admin_controller/list_contact_us';
$route['account'] = 'front/account_page';
$route['account/upload'] = 'front/upload_page';
$route['front-upload'] = 'front/upload_doc';


// $route['upload-attachment'] = 'front/upload_attachments_by_ajax';



$route['about-us'] = 'front/aboutus';
$route['giftcards'] = 'front/giftCard';
$route['sign-up'] = 'front/signup';
$route['pages/(:any)'] = 'front/pages/$1';
$route['delete-users/(:num)'] = 'admin_controller/delete_users/$1';
$route['delete-contact/(:num)'] = 'admin_controller/delete_contactus/$1';
$route['change-password'] = 'front/updatePassword';
$route['forgot-password'] = 'admin_controller/forgot_password';
$route['reset-password'] = 'front/reset_password';

$route['giftcard'] = 'admin_controller/get_card';
$route['giftcard-coupons'] = 'admin_controller/get_card';

$route['add-giftcard'] = 'admin_controller/gift_card';
$route['update-giftcard/(:num)'] = 'admin_controller/update_giftcard_page/$1';

$route['add-coupan'] = 'admin_controller/view_giftcard_coupon';
$route['coupons'] = 'admin_controller/get_listcoupon';
$route['update-coupon/(:num)'] = 'admin_controller/update_coupon_page/$1';

$route['discount/(:any)'] = 'front/discount/$1';
$route['admin/activate'] = 'admin_controller/activate';
$route['admin/deactivate'] = 'admin_controller/deactivate';

