<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/AdminLTE.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/skins/_all-skins.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Gift cards
        <!-- <small>Preview</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('index.php/add-page') ?>">add-giftcard</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New</h3>
              <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
    <?php } ?>

    <?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="form" action="<?php echo base_url('index.php/admin_controller/add_card/');?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" name="title" class="form-control" value="<?php if ($this->session->flashdata('error')){ echo $this->session->userdata('title');}elseif(isset($result)){ echo $result['title'];}?>" id="title" placeholder="Enter page title">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Discount</label>
                  <input type="text" name="discount_per" class="form-control" value="<?php if ($this->session->flashdata('error')){ echo $this->session->userdata('discount_per');}elseif(isset($result)){ echo $result['discount_per'];}?>" id="discount_per" placeholder="Enter discount">
                </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">Image</label>
                  <input id="image" name="image" class="form-control" placeholder="" type="file">
                </div>

<!--                 <div class="form-group">
                  <label for="exampleInputPassword1">Description</label>
                  <textarea id="editor1" name="content" rows="10" cols="80"><?php// if ($this->session->flashdata('error')){//  echo $this->session->userdata('content'); }elseif(isset($result)){ echo $result['content'];}?></textarea>
                </div> -->

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
          <!-- /.box -->
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script src="<?php echo base_url('/assets/bower_components/jquery/dist/jquery.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/fastclick/lib/fastclick.js');?>"></script>

  <script src="<?php echo base_url('/assets/js/adminlte.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/js/demo.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/ckeditor/ckeditor.js');?>"></script>

  <script src="<?php echo base_url('/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>

  <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>

// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $('#form').validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      title: {
        required: true,
      },
      discount_per: {
        required: true,
      }
    },
    // Specify validation error messages
    messages: {

      title: {
        required: "Please provide a page title",
      },
      discount_per: {
        required: "Please provide a Discount",
      },
      // email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});



</script>

  </body>
  </html>
