<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/AdminLTE.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/skins/_all-skins.min.css'); ?>">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style>
.btn-group>.btn+.dropdown-toggle {
    padding-right: 5px;
    padding-left: 5px;
    height: 34px;
}

.btn {
    display: inline-block;
    padding: 6px 4px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}

</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">

  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Users List
          <!-- <small>advanced tables</small> -->
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Tables</a></li>
          <li class="active">Users List</li>
        </ol>

      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">All users</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>Phone</th>
                     <th>Country</th>
                      <th>Telegram</th>
                     <th>Daily Amount</th>
                    <!-- <th>Password</th> -->
                    <th>Hear about us</th>
                    <th>Created At</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($final as $list){?>
                  <tr>
                    <td><?php echo $list['firstname'];?></td>
                    <td><?php echo $list['lastname'];?></td>
                    <td><?php echo $list['email'];?></td>
                    <td><?php echo $list['phone'];?></td>
                      <td><?php echo $list['country'];?></td>
                    <!-- <td><?php //echo  $list['password'];?></td> -->
                    <td><?php echo $list['telegram'];?></td>
                    <td><?php echo $list['daily_amount'];?></td>
                    <td><?php echo $list['hear_about_us'];?></td>
                    <td><?php echo  date('F j, Y ', strtotime($list['created']));?></td>

                    <td>
                      <div class="btn-group">
                          <?php if(isset($list['activated_at']) && $list['activated_at'] != "") { ?>
                              <button type="button" class="btn btn-danger" onclick="deactivate(<?php echo $list['id'] ?>)">De Activate</button>
                          <?php }else { ?>
                              <button type="button" class="btn btn-success" onclick="activate(<?php echo $list['id'] ?>)">Activate</button>
                          <?php } ?>
                          <button type="button" class="btn btn-info">Action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                          <ul class="dropdown-menu" role="menu">
                            <!-- <li><a href="<?php //echo site_url('index.php/add-page/'.$list['id']);?>">Edit</a></li> -->
                            <li><a href="<?php echo site_url('delete-users/'.$list['id']);?>" onClick="return doconfirm();">Delete</a></li>
                          </ul>
                    </div>

              </td>
                  </tr>
                <?php }?>

                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>

<script src="<?php echo base_url('/assets/bower_components/jquery/dist/jquery.min.js');?>"></script>

<script src="<?php echo base_url('/assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>

<script src="<?php echo base_url('/assets/bower_components/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>

<script src="<?php echo base_url('/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>

<script src="<?php echo base_url('/assets/bower_components/fastclick/lib/fastclick.js');?>"></script>

<script src="<?php echo base_url('/assets/js/adminlte.min.js');?>"></script>

<script src="<?php echo base_url('/assets/js/demo.js');?>"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

<script>
function doconfirm()
{
    job=confirm("Are you sure to Delete?");
    if(job!=true)
    {
        return false;
    }
}

function deactivate(userid){
    $.ajax({
        url: '<?php echo site_url('admin/deactivate'); ?>',
        type: 'POST',
        data: {
            userid: userid
        },
        dataType: 'json',
        success: function(data) {
            alert('User successfully deactivated');
            window.location.reload()
        }
    });
}

function activate(userid){
    $.ajax({
        url: '<?php echo site_url('admin/activate'); ?>',
        type: 'POST',
        data: {
            userid: userid
        },
        dataType: 'json',
        success: function(data) {
            alert('User successfully activated');
            window.location.reload();
        }
    });
}
</script>

</body>
</html>
