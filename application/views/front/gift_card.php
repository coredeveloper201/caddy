<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Important Owl stylesheet -->
<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<div class="in-card">
  <div class="container">
  <h1 class="site-heading">Buy Discounted Gift Cards</h1>
  <p class="gift-p">
  Take advantage and buy other people's unwanted gift cards and codes online at a discount. <br>
  Pay less than you would have by paying with discounted cards at your favorite retailer. <br>
  Buying discounted gift cards online and saving money has never been easier!
  </p>

  <div class="row">
  <div class="col-xs-3" itemscope="" itemtype="http://schema.org/Product" searchterm="amazon account">
  <a itemprop="url" href="#" class="lnk-giftcard" title="Buy Discounted Amazon Account Online" alt="Amazon Account">
  <img itemprop="image" class="cart-image" src="<?php echo base_url('/assets/site/images/amazon-account.png'); ?>" alt="Amazon Account">
  <span class="desc">
  <span itemprop="brand" itemscope="" itemtype="http://schema.org/Brand">
  <span itemprop="name" class="cardname">Amazon Account</span>
  </span>
  <span class="light">Save up to 10%</span>
  </span>
  </a>
  </div>
  <div class="col-xs-3" itemscope="" itemtype="http://schema.org/Product" searchterm="home depot">
  <a itemprop="url" href="#" class="lnk-giftcard" title="Buy Discounted Home Depot Gift Cards Online" alt="Home Depot Gift Cards">
  <img itemprop="image" class="cart-image" src="<?php echo base_url('/assets/site/images/homedepot.png'); ?>" alt="Home Depot gift cards">
  <span class="desc">
  <span itemprop="brand" itemscope="" itemtype="http://schema.org/Brand"><span itemprop="name" class="cardname">Home Depot</span></span><span class="hidden" itemprop="name">Home Depot Gift Cards</span>
  <span class="light">Save up to 10%</span>
  </span>
  </a>
  </div>
  <div class="col-xs-3" itemscope="" itemtype="http://schema.org/Product" searchterm="target">
  <a itemprop="url" href="#" class="lnk-giftcard" title="Buy Discounted Target Gift Cards Online" alt="Target Gift Cards">
  <img itemprop="image" class="cart-image" src="<?php echo base_url('/assets/site/images/target.png'); ?>" alt="Target gift cards">
  <span class="desc">
  <span itemprop="brand" itemscope="" itemtype="http://schema.org/Brand"><span itemprop="name" class="cardname">Target</span></span><span class="hidden" itemprop="name">Target Gift Cards</span>
  <span class="light">Save up to 12%</span>
  </span>
  </a>
  </div>
  <div class="col-xs-3" itemscope="" itemtype="http://schema.org/Product" searchterm="best buy">
  <a itemprop="url" href="#" class="lnk-giftcard" title="Buy Discounted Best Buy Gift Cards Online" alt="Best Buy Gift Cards">
  <img itemprop="image" class="cart-image" src="<?php echo base_url('/assets/site/images/bestbuy.png'); ?>" alt="Best Buy gift cards">
  <span class="desc">
  <span itemprop="brand" itemscope="" itemtype="http://schema.org/Brand"><span itemprop="name" class="cardname">Best Buy</span></span><span class="hidden" itemprop="name">Best Buy Gift Cards</span>
  <span class="light">Save up to 6%</span>
  </span>
  </a>
  </div>
  </div>
  </div>
</div>

</body>
</html>
