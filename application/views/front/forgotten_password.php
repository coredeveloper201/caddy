<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Important Owl stylesheet -->
<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

<!-- Header -->
<div class="header">
  <header class="header-section section sticker header-transparent for-notification-bar">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 col-sm-6 col-xs-6">
          <div class="header-logo">

            <a href="<?php echo base_url('index.php/front');?>" class="white-logo">
              <img src="<?php echo base_url('assets/site/images/logo.png');?>" alt="logo">
            </a>
            <!-- <a href="/" class="dark-logo">
              <img src="//storage.googleapis.com/redeeem/assets/images/logo_regular.png" alt="logo">
            </a> -->

          </div>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-6 float-right">
          <div class="header-option-btns float-right">

  		  <!-- DROPDOWN MENU -->

            <div class="header-account float-left">
              <ul>
                <li class="">

                  <a href="#" data-toggle="dropdown" aria-expanded="false">
                    <i class="pe-7s-config"></i>
                  </a>

  				<ul class="dropdown-menu">

                    <li>
                    	<a href="/pages/about">
                        About
                      </a>
                    </li>

                    <li>
                    	<a href="/collections/amazon-gift-cards?sort_by=created-ascending">
                        Offers
                      </a>
                    </li>

                    <li>
                    	<a href="/pages/guarantee">
                        Guarantee
                      </a>
                    </li>

                    <li>
                    	<a href="/products/redeeem-gift-card">
                        Gift Cards
                      </a>
                    </li>

                    <li>
                    	<a href="/pages/bitpay">
                        BitPay
                      </a>
                    </li>






                    <li>
                        <a href="/pages/kyc">
                          KYC Process
                        </a>
                    </li>







                      <li>
                          <a href="<?php echo base_url('index.php/front/signup')?>">
                            Signup
                        	</a>
                      </li>
                      <li>
                          <a href="/account/login">
                            Login
                        	</a>
                      </li>


                  </ul>
                </li>
              </ul>
            </div>
            <!-- Header Cart -->

  <div class="header-cart float-left">

    <a class="cart-toggle" href="#" data-toggle="dropdown">
      <i class="pe-7s-cart"></i>
      <span class="bigcounter">0</span>
    </a>

    <div class="mini-cart-brief dropdown-menu text-left">

      <div class="w-100 mt3">
        <p class="f3 tc">

          	You have <span class="fw7">0</span> items in your cart.

        </p>
      </div>

      <hr class="mt4 mb0">

      <div class="all-cart-product mt4 clearfix">
        <ul>

        </ul>
      </div>

      <div class="h2 ph4">
        <span class="fl tc f3 ph4">Total</span>
        <span class="fr tc f3 ph4 success">$0.00</span>
      </div>
      <hr class="pb2">

      <div class="cart-bottom w-60 clearfix center">
        <a href="/cart">Go to Cart</a>
      </div>

      <a href="/cart/clear" class="clear-cart dark-gray mt4 mb3 f4 tc db lh-solid">Empty Cart</a>

    </div>
  </div>
          </div>
        </div>
        <!-- primary-menu -->
        <div class="col-md-8 col-xs-12">
          <nav class="main-menu text-center" style="display: block;">
    <ul>
        <li><a href="/pages/about">BUY GIFT CARDS</a></li>
        <li><a href="/collections/amazon-gift-cards">SELL GIFT CARDS</a></li>
        <li><a href="/pages/guarantee">contact us</a></li>
      	<li><a href="/account/register">Signup</a></li>
      	<li><a href="/account/login">Login</a></li>
    </ul>
  </nav>
          <div class="mobile-menu"></div>
        </div>
      </div>
    </div>
  </header>
</div>
<!-- header-end -->

<main role="main">
		<!-- BEGIN content_for_index -->
  <div id="shopify-section-1516534241377" class="shopify-section">
      <div class="home-slider-section">

     <div id="home-slider" class="cover">

       <div class="hero-slider-content tc pt7">

         <h1 class="white fw7 ma0 lh-title">
           Gift Cards Buying &amp; Selling Made Easy
         </h1>

         <p class="near-white f2-ns f3 pa3 ma0 lh-title">
           Buy Discounted Gift Cards<br>on CardBazaar.
         </p>
         <p class="near-white f2-ns f3 pa3 ma0 lh-title">
           Greater discounts, super easy to use, most popular brands
         </p>


           <a href="/account/register" class="hero-call-to-action">
             <i class="fal fa-globe-africa pr2"></i>
             Get Started
           </a>

       </div>
           </div>
         </div>
     </div>

	</main>
<!-- END content_for_index -->
<div class="site-section">
  <div class="container">
    <div  class="login-container">

      <div class="block push-bit">

      <form action="#" method="post" id="form-register" class="form-vertical  display-none" novalidate="novalidate" style="display: block;">
      <div class="title-bordered">Reset password here!</div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Email</label>
      <input id="email" name="email" class="form-control" placeholder="Email" required="true" type="email" aria-required="true">
      </div>
      <div class="form-group form-actions">
      <button type="submit" class="btn btn-wide btn-lg btn-success">Reset password</button>
      </div>
      </form>

      </div>

    </div>
  </div>
</div>
<div class=footer>
  <div id="shopify-section-footer" class="shopify-section">
  <div class="footer-top-section section pt-100 ph4">
    <div class="container">
      <div class="row">
        <div class="footer-widget col-md-4 col-sm-6 col-xs-12 mb-40">
          <h4 class="widget-title">
            <a href="/" class="white-logo">
              <img src="<?php echo base_url('assets/site/images/logo.png');?>" width="150" alt="logo">
            </a>
          </h4>
          <p class="mb2">
            <strong class="pr3">Address</strong>
            <span>8939 Sepulveda Blvd<br>Los Angeles, CA 90045</span>
          </p>
          <p class="mb2">
            <strong class="pr3">Email</strong>
            <span>support@cardbazaar.io</span>
          </p>
          <!-- <p class="mb2">
            <strong class="pr3">Chat</strong>
            <a href="#shopify-section-footer" id="footer-chat" class="link">Start Chat</a>
          </p> -->
        </div>

        <div class="footer-widget link-widget col-md-4 col-sm-6 col-xs-12 mb-40">
          <h4 class="widget-title">
            Directory
          </h4>
          <ul>




            	<li>
                <a class="fot-a" href="/pages/about">About Us</a>
            	</li>

            	<li>
                <a class="fot-a" href="/collections/amazon-gift-cards">Contact Us</a>
            	</li>

            	<li>
                <a class="fot-a" href="/pages/guarantee">Terms of Use</a>
            	</li>
<!--
            	<li>
                <a class="fot-a" href="/pages/kyc">KYC Process</a>
            	</li> -->

            	<li>
                <a class="fot-a" href="/products/redeeem-gift-card">Privacy Policy</a>
            	</li>

            	<li>
                <a class="fot-a" href="/pages/bitpay">Spend Policy</a>
            	</li>

            	<li>
                <a class="fot-a" href="/pages/contact">Refund Policy</a>
            	</li>




          </ul>
        </div>

        <div class="footer-widget col-md-4 col-sm-6 col-xs-12 mb-40">

          <h4 class="widget-title">
            Community
          </h4>

          <p class="widget-p">Get notified instantly in Telegram as soon as new gift cards are added to the site.</p>
<!--
          <a href="https://t.me/redeeem" target="_blank" class="light-purple">
            <i class="fab fa-telegram pr3"></i>Join us on Telegram
          </a> -->

        </div>
      </div>
    </div>
  </div>
<!--
  <script>
    $("#footer-chat").click(function() {
      event.stopPropagation();
      Intercom('show');
    });
  </script> -->

  <div class="footer-middle-section section">
    <div class="container">
      <div class="row">
        <div class="db ph4">
          <a href="/pages/bitpay" class="dim">
            <img src="//storage.googleapis.com/redeeem/assets/images/btc_payment.png" class="dib ph2-ns" alt="Bitcoin">
          </a>
          <a href="/pages/bitpay" class="dim">
            <img src="//storage.googleapis.com/redeeem/assets/images/bch_payment.png" class="dib ph2-ns" alt="Payment">
          </a>
          <a href="//www.trustpilot.com/review/redeeem.com" target="_blank" class="dim">
            <img src="https://storage.googleapis.com/redeeem/assets/images/trustpilot.png" class="dib pt3 pt0-ns ph3-ns" alt="TrustPilot">
          </a>
        </div>
      </div>
    </div>
  </div>

<div class="main">


</div>
  <!-- FOOTER-SUB SECTION START -->
  <div class="footer-bottom-section section pv5">
    <div class="container">
      <div class="row">
        <div class="copyright ph4">
          <p class="gray f4 ma2 lh-title">All content © 2018 Card Bazaar LLC. CardBazaar and the CardBazaar logo are registered trademarks belonging to Card Bazaar LLC. All trademarks not owned by CardBazaar that appear on this site are the property of their respective owners. CardBazaar is not the issuer of any of the gift cards or other closed-loop products on CardBazaar and is not related to any merchant whose trademarks and gift cards appear on CardBazaar for sale</p>
          <p class="gray f4 ma2 lh-title">Redeeem is <strong>not</strong> an official partner of Amazon.com, Inc, or its affiliates.</p>
          <p class="mid-gray f4 ma2 lh-title">
            <a href="/pages/terms" class="o-70">Terms</a> • <a href="/pages/privacy" class="o-70">Privacy</a>
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- FOOTER-SUB SECTION END -->

  </div>
</div>


<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>

// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $('#form').validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      guest_name: "required",
      designation : "required",
      address : "required",
      booking_date : "required",
      checkin_date : "required",
      // checkout_date : "required",
      relationship : "required",


      contact: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        number: true,
        maxlength: 10
      },
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      register_id: {
        required: true,
        number: true
      }
    },
    // Specify validation error messages
    messages: {
      guest_name: "Please provide a Name",
      designation: "Please provide a Designation",
      address: "Please enter a Address",
      booking_date: "Please select a Booking date",
      checkin_date: "Enter  Check-in date",
      checkout_date: "Enter Chek-out date",
      relationship: "Please provide a Relationship name and address",

      contact:{
        required: "Please provide a contact number",
        number: "Please enter a valid  contact number"
      },
      register_id: {
        required: "Please provide a registration number",
        number: "Please enter a valid  number"
      },
       email:{
        required: "Please provide a email",
        email: "Please enter a valid email address"
      },
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});

        </script>
</body>
</html>
