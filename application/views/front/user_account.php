<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Important Owl stylesheet -->
<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

<div class="site-section">
   <div class="container">
        <?php if ($this->session->flashdata('success')) { ?>
<div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
<?php } ?>

    <?php if ($this->session->flashdata('error')) { ?>
<div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
<?php } ?>
    <div class="main-content">
              <h1 class="my-account">My Account</h1>
      <div class="row">
        <div class="col-md-3">
 <?php $this->load->view('front/left_menus'); ?>
      </div>
      <div class="col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" style="text-transform: uppercase;"><?php echo $this->session->userdata('firstname');?></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>firstname</td>
                        <td><?php echo $this->session->userdata('firstname');?></td>
                      </tr>
                      <tr>
                        <td>lastname</td>
                        <td><?php echo $this->session->userdata('lastname');?></td>
                      </tr>
                      <tr>
                      <tr>
                        <td>Email</td>
                        <td><?php echo $this->session->userdata('email');?></td>
                      </tr>
                        <tr>
                        <td>Phone Number</td>
                        <td><?php echo $this->session->userdata('phone');?></td>
                      </tr>
                       <tr>
                        <td>Telegram Name</td>
                        <td><?php echo $this->session->userdata('telegram');?></td>
                      </tr>
                       <tr>
                        <td>Daily Amount</td>
                        <td><?php echo $this->session->userdata('daily_amount');?></td>
                      </tr>


                    </tbody>
                  </table>
<!--
                  <a href="#" class="btn btn-primary">My Sales Performance</a>
                  <a href="#" class="btn btn-primary">Team Sales Performance</a> -->
                </div>
              </div>
            </div>

          </div>
      </div>
      </div>
  </div>

</div>

</div>
</body>
</html>
