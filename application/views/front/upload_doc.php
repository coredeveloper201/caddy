<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Important Owl stylesheet -->
	<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

	<!-- Default Theme -->
	<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

	<!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src='https://code.jquery.com/jquery-3.3.1.js'></script>
<style>
form h2 {
  color: #379e01;
  font-size: 25px;
  padding: 50px 0;
}
form {
  width: 80%;
  margin: 0 auto;
}
.heading-div {}
.heading-div label {
  color: #379e01;
}
.heading-div input {
  display: block;
  text-align: center;
  width: 100%;
  margin: 0 auto;
  border: 2px solid #a3a3a3;
  padding: 5px;
  color: #666;
  font-weight: bold;
}
input#image_upload {
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
}
.image-div {}
input#image_upload + label {
    font-size: 1.25em;
    cursor: pointer;
    font-weight: 700;
    color: #379e01;
    padding: 5px;
    background-color: transparent;
    border: 2px solid #a3a3a3;
    width: 100%;
    display: block;
    text-align: center;
}

input#image_upload:focus + label {}
input#image_upload + label:hover {
    background-color: gray;
    color: white;
}
#image_upload + label * {
  pointer-events: none;
}
button.submit {
 background: #379e01;
 color: white;
 border: none;
 padding: 5px 10px;
 outline: none;
}

#selectedFiles li {
  color: gray;
  font-size: 14px;
}

#selectedFiles li span{
  padding-left: 20px;
  cursor: pointer;
  display: inline-block;
}
#selectedFiles li span:hover{
  color: tomato;
}
</style>

</head>
<body>

<div class="site-section">
	<div class="container">
		<div class="main-content">
			<h1 class="my-account">Document Upload</h1>
			<div class="row">
				<div class="col-md-3">
					<?php $this->load->view('front/left_menus'); ?>
				</div>
				<div class="col-md-9">
					<div  class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title" style="text-transform: uppercase;"><?php echo $this->session->userdata('firstname');?></h3>
						</div>
						<div class="panel-body">
                  <form
                    data-action="<?php echo base_url('front/upload_doc_ajax/');?>"
                   role="form" id="form" action="<?php echo base_url('front/upload_doc/');?>" method="POST" enctype="multipart/form-data">
                      <div class="box-body">

                          <div class="form-group heading-div">
                              <label for="exampleInputEmail1">Heading</label>
                              <input id="heading_input" type="text" name="heading" placeholder="Enter Heading" value="<?php if(isset($user['heading'])) echo $user['heading']; ?>" id="heading" placeholder="Enter Heading">
                          </div>

                          <div class="form-group image-div">
                              <input
                                data-action="<?php echo base_url('front/upload_attachments_by_ajax/');?>"
                                data-attachments="<?php echo base_url('front/attachments_by_clients_web_id/');?>"
                                data-image_root="<?php echo base_url('/assets/user/images/');?>"
                                data-delete="<?php echo base_url('front/delete_attachment/');?>"
                                id="image_upload"
                                multiple
                                name="image"
                                placeholder=""
                                type="file">
                              <label id="image_upload_label" for="image_upload"><i class="fa fa-paperclip" aria-hidden="true"></i> Add File</label>
                          </div>
                          <div class='form-group'>
                              <div id="selectedFiles"></div>
                          </div>
                          <!--


                           -->

                          <!-- <?php // if(isset($user['upload_file']) && $user['upload_file'] != "") {?> -->
                              <!-- <img src="<?php // echo base_url('/assets/user/images/'.$user['upload_file']); ?>"> -->
                          <!-- <?php // }?> -->

                          <!--                 <div class="form-group">
<label for="exampleInputPassword1">Description</label>
<textarea id="editor1" name="content" rows="10" cols="80"><?php// if ($this->session->flashdata('error')){//  echo $this->session->userdata('content'); }elseif(isset($result)){ echo $result['content'];}?></textarea>
</div> -->

                      </div>
                      <!-- /.box-body -->

                      <div class="box-footer">
                          <button type="submit" class="submit">Submit</button>
                      </div>
                  </form>

						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<script>
(function () {
var app = {
  files: [

  ],
  loading: false,
  render: function () {
    var html = '';
    this.files.forEach(file => {
      html+= `
      <li>
        <i class="fa fa-paperclip" aria-hidden="true"></i>
        <a href='${this.$input.attr('data-image_root') + file.path }' download>
          ${this.text_truncate(file.name, 25)}
        </a>
        <span class='close-file' data-id=${file.id}>X</span>
      </li>`;
    });
    this.$selectedFiles.html(html);
  },
  init: function () {
    this.domCached();
    this.bindThis();
    this.bindEvents();
    // this.onLoadFiles();
    this.render();
  },
  onLoadFiles: function() {
    var url = this.$input.attr('data-attachments');
    $.ajax({
      type: 'GET',
      url,
      success: (response) => {
        let data = JSON.parse(response);
        this.files = data;
        this.render();
      }
    })
  },
  bindThis: function() {
    this.fileChange = this.fileChange.bind(this);
  },
  domCached: function () {
   this.$input = $('#image_upload');
   this.$label = $('#image_upload_label');
   this.label_spinner = '<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>'
   this.label_content = `<i class="fa fa-paperclip" aria-hidden="true"></i> Add File`
   this.$selectedFiles = $('#selectedFiles');
   this.$form = $('#form')
   this.$heading_input = $('#heading_input');
  },
  bindEvents: function () {
    this.$input.on('change', this.fileChange);
    this.$form.on('click', '.close-file', this.deleteFile.bind(this));
    this.$form.on('submit', this.submit.bind(this));
  },
  submit: function (e) {
    e.preventDefault();
    var heading = this.$heading_input.val();
    var attachments = [];
    this.files.forEach(file => {
      attachments.push(file.id);
    })
    var data = {
      heading,
      attachments: attachments.join(',')
    }
    this.formSubmit(data)
  },
  formSubmit: function (data) {
    $.ajax({
      type: 'POST',
      data,
      url: this.$form.attr('data-action'),
      success: (response) => {
        this.$form.html(
          `<h2>Document uploaded successfully. We are processing ASAP, You will hear back Shortly</h2>`
          )
      }
    })
  },
  deleteFile: function(e) {
    var id = $(e.currentTarget).attr('data-id');
    this.files = this.files.filter(file => file.id != id);
    this.render();
    $.ajax({
      type: 'POST',
      url: this.$input.attr('data-delete'),
      data: {id: id},
      success:  (response) => {
        console.log('removed')
      }
    })
  },
  fileChange: function (e) {
    e.preventDefault();
    var files = e.target.files;
    console.log('flies', files);
    for (var i = 0; i < files.length; i++) {
        var eachFile = files[i];
        var formData = new FormData();
        formData.append("image", eachFile);
        this.uploadThisFile(formData);
    }
  },
  uploadThisFile: function (formData) {
     this.$label.html(this.label_spinner);
    $.ajax({
        type: "POST",
        url: this.$input.attr('data-action'), // Upload URL Here
        data: formData,
        processData: false,
        contentType: false,
        xhr:  () => {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress",  (evt) => {
                if (evt.lengthComputable) {
                    //=================================================
                    // this is the upload progress in percentage
                    //=================================================
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    console.log('percentComplete', percentComplete);
                    this.$label.html(this.label_spinner + ' ' + percentComplete + '%');
                    if (percentComplete > 99) {
                      setTimeout(() => {
                         this.$label.html(this.label_content);
                      }, 1000)
                    }
                }
            }, false);
            return xhr;
        },
        success:  (response) => {
            //=================================================
            // Ajax return Response
            //=================================================
            this.files.push(JSON.parse(response))
            this.render();
        }
    });

  },
  text_truncate: function(str, length, ending) {
    if (length == null) {
      length = 100;
    }
    if (ending == null) {
      ending = '...';
    }
    return str.length > length ? str.substring(0, length - ending.length) + ending : str;
  },

}
app.init();
}());

// $('#image_upload').on('change', function (e) {
//   $file = $('#image_upload').val();
//   console.log($file);
// })

// input.onchange = function () {
//   var file = input.files[0];
//   drawOnCanvas(file);   // see Example 6
//   displayAsImage(file); // see Example 7
// };

</script>
</body>
</html>
