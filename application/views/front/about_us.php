<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Important Owl stylesheet -->
<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

<div class="site-section">
  <div class="container">
    <div  class="">
        <div class="about-text">

          <div class="about-text-p">

            <?php echo $data->content;?>
          </div>

      </div>

    </div>
  </div>
</div>
<style>
    .about-text-p>h1{
        background-color: #4273B5;
        padding:40px;
        color:#fafaf4 !important;
        border:none;
    }
</style>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
</body>
</html>
