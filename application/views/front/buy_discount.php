<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Important Owl stylesheet -->
<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style>
.site-navbar {
    border-radius: 0;
    border-width: 2px 0;
    border-style: solid;
    border-color: #98999a;
    background-color: #f6f8f7;
    line-height: 46px;
    margin: 0;
}

.site-heading {
    text-align: left;
}


a.btn-tier.btn-tier-1 {
    padding: 3px;
    background-image: linear-gradient(to right top, #5795c1, #4182bb, #326eb3, #2e59a9, #35439b);
    font-size: 12px;
    color: white;
    border-radius: 3px;
}

a.btn-tier.btn-tier-2 {
    padding: 3px;
    background-image: linear-gradient(to right top, #5795c1, #4182bb, #326eb3, #2e59a9, #35439b);
    font-size: 12px;
    color: white;
    border-radius: 3px;
}

a.btn-tier.btn-tier-3 {
    padding: 3px;
    background-image: linear-gradient(to right top, #5795c1, #4182bb, #326eb3, #2e59a9, #35439b);
    font-size: 12px;
    color: white;
    border-radius: 3px;
}

.Discounted-section{background-color:#f5f5f5;}
.Discounted-section .giftcards-container .site-heading{color:#35439b;}
.giftcards-footer .notes {
    width: 100%;
    background-color: #f6f8f7;
    border: 1px solid #98999a;
    font-size: 11px;
    padding: 10px;
    margin: 10px 0;
}
.giftcards-footer {
    border-top: 2px solid #ddd;
	background: #f5f5f5;
}
</style>

</head>
<body>

<div class="navbar site-navbar">
<div class="container" itemprop="breadcrumb">
<a href="<?php echo base_url('/');?>">Home</a> / <a href="#" title="Buy Gift Cards">Buy Gift Cards</a> / <?php echo $title; ?>
</div>
</div>


<div class="site-section nopadding Discounted-section">
  <div class="container">
  <div class="row">
  	
  <div class="col-xs-12 col-md-4 giftcards-container-sidebar">
		<img itemprop="image" src="<?php echo base_url() .'assets/admin/images/'.$card->image; ?>" alt=" Walmart gift cards" style="max-width: 100%;">
		<div class="walmart">
		<!-- <a href="#" title="Sell Gift Cards"><small>Sell <?php //echo $title; ?> Gift Cards at CardCaddy</small></a> -->
		</div>
  </div>

  <div class="col-xs-12 col-md-8 giftcards-container">
  	<h1 class="site-heading">Buy Discounted <?php echo $title; ?> Gift Cards</h1>
  	<p itemprop="description">Buy discounted <?php echo $title; ?> gift cards online. Freely spend it in store or online, faster you spend the more you save.</p>

<!--   	<div>
	Spend Policy: <a href="#" class="btn-tier btn-tier-1">Same day</a> <a href="#" class="btn-tier btn-tier-2">7 Days</a> <a href="#" class="btn-tier btn-tier-3">30 Days</a>
	<a href="#" target="_blank" title="What is Spend Policy?" class="btn-tier btn-question-mark"><img alt="" src="<?php //echo base_url('/assets/site/images/2018-11-03.png');?>" style="height: 22px;margin: -5px 0 0;"></a>
	</div> -->

	<!--<div style="margin-top: 10px;">
	Payment options:
	<img alt="" src="<?php /*echo base_url('/assets/site/images/BTC-icon.png');*/?>" style="height: 30px;">
	<img alt="" src="<?php /*echo base_url('/assets/site/images/e-check.png');*/?>" style="height: 22px;margin: 0px 0 0;">
	<a href="#" target="_blank" title="Payment options" class="btn-tier btn-question-mark"><img alt="" src="<?php /*echo base_url('/assets/site/images/2018-11-03.png');*/?>" style="height: 22px;margin: -5px 0 0;"></a>
	</div>-->
  
	<div style="margin-top: 10px;">
	Looking to purchase bulk?
	<a href="#" rel="nofollow">Check out our auto fill tool!</a>
	</div>

	<div id="giftcards-table-wrapper">

      <table class="giftcards-table">
      	<tbody>
      		<tr>

      		  <th class="hidden-xs"></th>  
      		  <th class="hidden-xs">Type</th>
      		  <th>Spend Policy</th>
      		  <th>Value</th>
      		  <th>Qty</th>
      		  <!--<th><img alt="" src="<?php /*echo base_url('/assets/site/images/e-check.png');*/?>" style="height: 40px;margin: 5px 0;padding-bottom: 10px;"></th>
      		<th><img alt="" src="<?php /*echo base_url('/assets/site/images/BTC-icon.png');*/?>" style="height: 30px;margin: 5px 0;"></th>-->
      		 <th></th>

      		</tr>

           <?php foreach( $coupon as $data){?>
      		<tr class="flt-type-walmart flt-tier-1 flt-cardId-cbzvfqsj">
          
      			<td class="hidden-xs"><img class="img-preview" src="<?php echo base_url() .'assets/admin/images/'.$data->image; ?>" alt="walmart gift cards"></td>
      			<td class="hidden-xs"><span class="bordered"><?php echo $data->type; ?></span></td>
      			<td><span class="btn-tier btn-tier-1 active"><?php echo $data->spend_policy; ?></span></td>
      			<td>$<?php echo $data->value; ?></td>
      			<td><?php echo $data->quantity; ?></td>
      			<!--<td><?php /*echo $data->payment_opt1; */?></td>
      			<td><?php /*echo $data->payment_opt2; */?></td>-->
      			<td><a href="<?php echo base_url('sign-up');?>" onclick="cartAdd('walmart', '1', 'cbzvfqsj'); return false;" class="btn btn-success">Buy now</a></td>
            
      		</tr>
          <?php }?>
      	</tbody>
      </table>

	</div>

  </div>


  </div>
  </div>
</div>

<section class="site-content site-section giftcards-footer">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-4">
<b>&nbsp;</b>
</div>
<div class="col-xs-12 col-md-8">
<b>CardCaddy</b>
<div class="notes">
For questions about redeeming gift cards, contact support@cardcaddy.co.
</div>
<p>
All trademarks not owned by CardCaddy that appear on this site are the property of their respective owners.<br>
<a href="https://cardcaddy.co/pages/terms-of-use" title="Terms of Use">+ See More</a>
</p>
</div>
</div>
</div>
</section>



<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
</body>
</html>
