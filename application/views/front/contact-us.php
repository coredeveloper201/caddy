<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Important Owl stylesheet -->
<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.site-section .container {
    max-width: 500px;
}
</style>
</head>
<body>

<div class="site-section">
  <div class="container">
            <?php if ($this->session->flashdata('error')) { ?>
<div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
<?php } ?>
    <h1>Contact Us</h1>
    <p class="contact-us">Have any questions? Leave us a message and you will be contacted by one of our customer service agents shortly</p>
    <div  class="login-container">

      <div class="block push-bit">

      <form action="<?php echo base_url('front/contact_us'); ?>" method="post" id="form" class="form-vertical" style="display: block;">
      <div class="form-group">
      <label class="control-label" aria-required="true">Full name</label>
      <input id="fullname" name="fullname" class="form-control" placeholder="Full name"  type="text" >
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Email</label>
      <input id="email" name="email" class="form-control" placeholder="Email"  type="email" >
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Phone</label>
      <input id="phone" name="phone" class="form-control" placeholder="Phone"  type="text" >
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Message</label>
      <textarea id="message" name="message" class="form-control" placeholder=" Enter your message" ></textarea>
      </div>

      <div class="form-group form-actions">
      <button type="submit" name="btnsubmit" class="btn btn-wide btn-lg btn-success">Submit</button>
      </div>

      </form>

      </div>

    </div>
  </div>
</div>

<!-- 
<section class="site-section page-generic text-center aboutus">
<div class="container">
<p>Card Bazaar LLC<br>
support@cardbazaar.io<br>
8 The Green STE A, Dover&nbsp;DE 19901</p>
<p>&nbsp;</p>
</div>
</section> -->





<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>

// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $('#form').validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      fullname: "required",
      message : "required",

      email: {
        required: true,
        email: true
      },

      phone: {
        required: true,
        minlength: 10,
        maxlength: 10,
        number: true
      },
    },
    // Specify validation error messages
    messages: {
      fullname: "Please provide a fullname",
      message: "Please provide a message",

      phone:{
       required: "Please provide a phone number",
       minlength: "Phone number must be only 10 digits",
       number: "Please enter a valid  phone number"
      },

       email:{
        required: "Please provide a email",
        email: "Please enter a valid email address"
      },
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});
  </script>
</body>
</html>
