<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Important Owl stylesheet -->
<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script>
 function disableSubmit() {
  document.getElementById("submit").disabled = true;
 }

  function activateButton(element) {

      if(element.checked) {
        document.getElementById("submit").disabled = false;
       }
       else  {
        document.getElementById("submit").disabled = true;
      }

  }
</script>


<style>

label.not-label:after {
    content: none;
    color: red;
    margin-left: 5px;
}

a.fot-b {
    color: #999;
    text-decoration: none;
    /* font-weight: 600; */
    font-size: 16px;
}

</style>

</head>
<body>

<div class="site-section">
  <div class="container">
        <?php if ($this->session->flashdata('error')) { ?>
<div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
<?php } ?>
    <div  class="login-container">

      <div class="block push-bit">

      <form action="<?php echo base_url('signup'); ?>" method="post" id="form" class="form-vertical" style="display: block;" enctype="multipart/form-data">
      <div class="title-bordered"><h2>Request exclusive Club access!</h2>
       <p class= "signup">We are accepting request to join the Card Caddy Club to be a member of our community.</p>
      </div>

      <div class="form-group">
      <label class="control-label" aria-required="true">First name</label>
      <input id="firstname" name="firstname" class="form-control" placeholder="First name"  type="text" >
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Last name</label>
      <input id="lastname" name="lastname" class="form-control" placeholder="Last name"  type="text" >
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Email</label>
      <input id="email" name="email" class="form-control" placeholder="Email"  type="email" >
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Password</label>
      <input id="password" name="password" class="form-control" placeholder="Password"  type="Password" >
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Country</label>
      <input id="country" name="country" class="form-control" placeholder="Country"  type="text" >
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Phone</label>
      <input id="phone" name="phone" class="form-control" placeholder="Phone"  type="text" >
      </div>

      <div class="form-group">
      <label class="control-label not-label" aria-required="true">Telegram name  </label>
      <input id="telegram" name="telegram" class="form-control" placeholder="Telegram name"  type="text" >
      </div>

      <div class="form-group">
      <label class="control-label">How did you hear about us?</label>
      <input id="hear_about_us" name="hear_about_us" class="form-control" placeholder="just a few words" type="text">
      </div>

      <div class="form-group">
      <label class="control-label">Daily amount needed</label>
      <input id="daily_amount" name="daily_amount" class="form-control" placeholder="Daily amount needed" type="text">
      </div>

<!--       <div class="form-group">
      <label class="control-label not-label">Add a file</label>
      <input id="upload_file" name="upload_file" class="form-control" placeholder="" type="file">
      </div> -->

      <div class="form-group">
      <label class="control-label">Confirm your age 18 and Up</label>
      <input type="checkbox" id="age" name="age">
      </div>

      <div class="form-group">
      <label class="control-label">I agree to <a class="fot-b" href="<?php echo base_url('pages/terms-of-use')?>">terms</a> stated on the website</label>
      <input type="checkbox" id="terms" name="terms">
      </div>

      <div class="form-group form-actions">
      <button type="submit" name="btnsubmit" class="btn btn-wide btn-lg btn-success">Request Club Access</button>
      </div>
      <div class="form-group text-center">
      Already have an account?  <a href="<?php echo base_url('login');?>" id="link-register">Enter</a>
      </div>
      </form>

      </div>

    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>

// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $('#form').validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      firstname: "required",
      lastname : "required",
      hear_about_us : "required",
      //telegram : "required",
      daily_amount : "required",
      country : "required",

      age : "required",
      terms : "required",

      email: {
        required: true,
        email: true
      },
      
      password: {
        required: true,
        minlength: 8
      },

      phone: {
        required: true,
        minlength: 10,
        maxlength: 10,
        number: true
      },
    },
    // Specify validation error messages
    messages: {
      firstname: "Please provide a firstname",
      lastname: "Please provide a lastname",
      hear_about_us: "Please enter  just a few words",
      //telegram : "Please enter a telegram name",
      daily_amount : "Please enter a daily amount",
      country : "Please provide a country name",
      age : "Please agree to  confirm your age",
      terms : "Please agree to terms stated on the website",

      password:{
       required: "Please provide a password",
       minlength: "Your password must be at least 8 characters long"
     },

       email:{
        required: "Please provide a email",
        email: "Please enter a valid email address"
      },

      phone:{
       required: "Please provide a phone number",
       minlength: "Phone number must be only 10 digits",
       number: "Please enter a valid  phone number"
      },
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});
  </script>
</body>
</html>
